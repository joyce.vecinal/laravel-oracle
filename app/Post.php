<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Comment;

class Post extends Model
{
  protected $guarded = ['id'];


  public function comments()
  {
    return $this->hasmany(comment :: class,'id');
  }

}
