<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ForexController extends Controller
{
    public function getUpdates(Request $request)
    {

    $client = new Client([
    'base_uri' => 'http://data.fixer.io',
    ]);

    $api_key=config('forex.api_key');

    $response =$client->request(
        'GET',
        "/api/latest?access_key=$api_key&base=EUR&symbols=USD,AUD,PHP,PLN,MXN"
    );

    $payload= json_decode ($response->getBody(), true);
    return $payload;
    //  return $payload['rates']['USD'];
        }
    }
