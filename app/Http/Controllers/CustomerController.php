<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerRequest;
use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    public function index()
    {
          return Customer::all();
    }

    public function create(CustomerRequest $request)
    {
        return Customer::create($request->all());
       
    }
    public function get(Customer $customer)
    {
        return $customer;
       
    }

  
    public function updateCustomer(CustomerRequest $request, $id)
    {
        $customer = Customer::findOrFail($id);
        if ($customer) {
            $customer->update($request->all());
            return $customer;
        }
    }
    

     public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        if ($customer) {
            $customer->delete();
            return $customer;
            return response()->json(['message' => 'ok'], 204);
           
        }
    }


    public function TransactionwithCustomer()
    {
    return Customer::with('transaction')->get();
    }
   
    public function relatedCustomer()
    {
        return Customer::where('first_name',request('first_name'))->get(); 
    }
       
    public function filterCustomer()
    {
        return Customer::where('first_name',request('first_name'))->get(); 
       
    }
    public function searchCustomer()
    {
        return Customer::where([
            'first_name'=>request('first_name'),
            'last_name'=>request('last_name')
            ])->get(); 
      
    }
}

  
