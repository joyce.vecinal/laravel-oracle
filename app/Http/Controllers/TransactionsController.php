<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransRequest;
use Illuminate\Http\Request;
use App\Trans;

class TransactionsController extends Controller
{
    public function index()
    {
          return Trans::all();
    }

    public function create(TransRequest $request)
    {
        return Trans::create($request->all());
       
    }

    public function get(Trans $transaction)
    {
        return $transaction;
       
    }
  
    public function update(TransRequest $request, $id)
    {
        $transaction = trans::findOrFail($id);
        if ($transaction) {
            $transaction->update($request->all());
            return $transaction;
    
        }
    }

     public function destroy($id)
    {
        $transaction = trans::findOrFail($id);
        if ($transaction) {
            $transaction->delete();
            return $transaction;
            return response()->json(['message' => 'ok'], 204);
           
        }
    }

   
    public function CustomerTransaction()
    {
    return trans::with('Customer')->get();
    }
       
}