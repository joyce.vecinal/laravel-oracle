<?php

namespace App\Http\Controllers;

use App\Http\Requests\LogsRequest;
use Illuminate\Http\Request;
use App\Logs;

class LogsController extends Controller
{
    public function index()
    {
          return Logs::all();
    }

 
    public function create(LogsRequest $request)
    {
        return Logs::create($request->all());
       
    }


    public function get(Logs $logs)
    {
        return $logs;
       
    }

  
    public function UpdateLogs(LogsRequest $request, $id)
    {
        $logs = Logs::findOrFail($id);
        if ($logs) {
            $logs->update($request->all());
            return $logs;
        }
    }
    

     public function destroy($id)
    {
        $logs = Logs::findOrFail($id);
        if ($logs) {
            $logs->delete();
            return $logs;
            return response()->json(['message' => 'ok'], 204);
           
        }
    }


  
    public function filterCustomer()
    {
        return Customer::where('first_name',request('first_name'))->get(); 
       
    }
    public function searchCustomer()
    {
        return Customer::where([
            'first_name'=>request('first_name'),
            'last_name'=>request('last_name')
            ])->get(); 
      
    }
}

  
