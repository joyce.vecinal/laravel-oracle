<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransactionRequest;
use Illuminate\Http\Request;
use App\Transaction;

class TransactionController extends Controller
{
    public function index()
    {
          return Transaction::all();
    }

    public function create(TransactionRequest $request)
    {
        return Transaction::create($request->all());
       
    }

    public function get(transaction $transaction)
    {
        return $transaction;
       
    }
  
    public function update(Request $request, $id)
    {
        $transaction = transaction::findOrFail($id);
        if ($transaction) {
            $transaction->update($request->all());
            return $transaction;
    
        }
    }

     public function destroy($id)
    {
        $transaction = Transaction::findOrFail($id);
        if ($transaction) {
            $transaction->delete();
            return $transaction;
            return response()->json(['message' => 'ok'], 204);
           
        }
    }

   
    public function CustomerTransaction()
    {
    return Transaction::with('Customer')->get();
    }
       
}