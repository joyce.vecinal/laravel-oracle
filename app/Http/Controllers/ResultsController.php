<?php

namespace App\Http\Controllers;

use App\Customer;
Use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;


class ResultsController extends Controller
{
    
    public function all()
    {
    return Customer::with(['transaction'=> function($query) {
    $query->where('service_charge',"1000.00");
    }])->get();
    }

public function filter(Request $request)
{
    return Customer::whereHas('transaction',function(Builder $query) use ($request){
        $query->where('service_charge',$request->service_charge);
    })->get();
}

    public function createTransaction(Request $request)
    {
        $customer = Customer::findorfail($request->id);
        if ($customer) {
            $transaction = $customer->transaction()->create([
                'principal_amount'=> $request->Principal_amount,
                    'service_charge' => $request->service_charge,
                    'remmitance_reason'=> $request->remmitance_reason
            ]);
            return $transaction;
        }
    }
    public function transactions($id)
    {
        return Customer::findorfail($id)->transaction;
    }

    public function updateTransaction(Request $request)
    {
        $customer =Customer::findorfail($request->customer_id);
        if ($customer){
            $customer->transaction()->findorfail($request->id)->update([
                'principal_amount'=> $request->Principal_amount,
                'service_charge' => $request->service_charge,
                'remmitance_reason'=> $request->remmitance_reason
                ]);
                return response()->json([
                    'message'=>'Transaction has been updated!'
                ]);
           
        }

    }
    public function  deleteTransaction($id)
{
 
    $customer =Customer ::findorfail($id);
    if($customer){
        $customer->transaction()->delete();
        return response()->json([],204);
    }
}
}
