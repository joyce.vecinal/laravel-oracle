<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use Illuminate\Http\Request;
use App\Comment;

class PostCommentController extends Controller
{
    public function index()
    {
          return Comment::all();
    }

    public function create(CommentRequest $request)
    {
        return Comment::create($request->all());
       
    }

    public function get(CommentRequest $comment)
    {
        return $comment;
       
    }

    public function update(CommentRequest $request, $id)
    {
        $comment = Comment::findOrFail($id);
        if ($comment) {
            $comment->update($request->all());
            return $comment;
    
        }
    }
     public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        if ($comment) {
            $comment->delete();
            return $comment;
            return response()->json(['message' => 'ok'], 204);
           
        }
    }
}