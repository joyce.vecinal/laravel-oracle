<?php

namespace App\Http\Controllers;

use App\Customer;
Use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;


class LogsCustomController extends Controller
{
    
    public function all()
    {
    return Customer::with(['Logs'=> function($query) {
    $query->where('customer_id',"4");
    }])->get();
    }


    public function filterLogs(Request $request)
    {
        return Customer::whereHas('Logs',function(Builder $query) use ($request){
            $query->where('customer_id',$request-> customer_id);
        })->get();
    }
    
   

    public function create(Request $request)
    {
        $customer = Customer::findorfail($request->customer_id);
        if ($customer) {
            $Logs = $customer->Logs()->create([
                'remarks'=> $request->remarks
            ]);
            return $Logs;
        }
    }

    public function logs($id)
    {
        return Customer::findorfail($id)->logs;
    }

    public function updateTransaction(Request $request)
    {
        $customer =Customer::findorfail($request->customer_id);
        if ($customer){
            $customer->Logs()->findorfail($request->id)->update([
                'customer_id'=> $request->customer_id,
                'remarks' => $request->remarks
                ]);
                return response()->json([
                    'message'=>'Transaction has been updated!'
                ]);
           
        }

    }
    public function  deleteTransaction($id)
{
 
    $customer =Customer ::findorfail($id);
    if($customer){
        $customer->Logs()->delete();
        return response()->json([],204);
    }
}
}
