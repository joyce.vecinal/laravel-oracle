<?php

namespace App\Http\Controllers;

use App\Http\Requests\PoatRequest;
use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    public function index()
    {
          return Post::all();
    }

    public function create(PoatRequest $request)
    {
        return Post::create($request->all());
       
    }

    public function get(Post $post)
    {
        return $post;
       
    }

    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        if ($post) {
            $post->update($request->all());
            return $post;
    
        }
    }
     public function destroy($id)
    {
        $post = Post::findOrFail($id);
        if ($post) {
            $post->delete();
            return response()->json(['message' => 'ok'], 204);
    
        }
    }

        public function allPostWithComments()
        {
        return Post::with('comments')->get();
        }
        public function filter()
        {
            return Post::where('title',request('title'))->get(); 
           
            // where operands
           // Post::where('title','>',request('title'))->get(); 
     
            // Post::where([
            //     ['title'-> 'test'],
            //     ['content'->'test']
            // ])->get();
        }

   public function searchField()
   {
       return Post::where([
           'title' => request('title'),
           'content'=>request('content')
       ])->get();

   }
   public function mixedSearch()
   {
       return Post::where('id', '=', 1) ->where([
           'title'=>request('title'),
           'content'=>request('content')
       ])->get();
   }

}

