<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Transaction;

class Customer extends Model
{
    protected $guarded = ['id'];

    public function transaction()
    {
      return $this->hasmany(transaction :: class,'customer_id');
    }
  
    public function logs()
    {
      return $this->hasmany(logs :: class,'customer_id');
    }
  

}