<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/posts','PostController@index');
Route::post('/posts','PostController@create');
Route::get('/posts/{post}','PostController@get');
Route::put('/posts/{id}','PostController@update');
Route::delete('/posts/{id}','PostController@destroy');
    

Route::get('/comment','PostCommentController@index');
Route::post('/comment','PostCommentController@create');
Route::get('/comment/{id}','PostCommentController@get');
Route::put('/comment/{id}','PostCommentController@update');
Route::delete('/comment/{id}','PostCommentController@destroy');

Route::get('/post-with-comments','PostController@allPostwithComments');
Route::get('/filter','PostController@filter');
Route::get('/search','PostController@searchField');
Route::get('/mixed-search','PostController@mixedSearch');




Route::get('/customer','CustomerController@index');
Route::post('/customer','CustomerController@create');
Route::get('/customer/{customer}','CustomerController@get');
Route::put('/customer/{id}','CustomerController@updateCustomer');
Route::delete('/customer/{id}','CustomerController@destroy');

Route::get('/trans-customer','CustomerController@TransactionwithCustomer');    
Route::get('/related-customer','CustomerController@relatedCustomer');
Route::get('/filter','CustomerController@filterCustomer');
Route::get('/search-customer','CustomerController@searchCustomer');

Route::get('/trans','TransactionsController@index');
Route::post('/trans','TransactionsController@create');
Route::get('/trans/{transaction}','TransactionsController@get');
Route::put('/trans/{id}','TransactionsController@update');
Route::delete('/trans/{id}','TransactionsController@destroy');

Route::get('/related-trans','TransactionsController@CustomerTransaction');
Route::get('/trans-customer','CustomerController@TransactionwithCustomer');

Route::get('/cust-trans','TransactionsController@CustomerTransaction');

Route::get('/results','ResultsController@all');
Route::get('/filter-results','ResultsController@filter');
Route::post('/add-transaction','TransactionController@create');
// Route::post('/add-transaction','ResultsController@createTransaction');
Route::get('/get-transactions/{id}','ResultsController@transactions');
Route::put('/update-transactions/{id}','ResultsController@updateTransaction');
Route::delete('/delete-transaction/{id}','ResultsController@deleteTransaction');

//Logs
Route::get('/Logs','LogsController@index');
Route::post('/Logs','LogsController@create');
Route::get('/Logs/{logs}','LogsController@get');
Route::put('/Logs/{id}','LogsController@updateCustomer');
Route::delete('/Logs/{id}','LogsController@destroy');


Route::get('/Logs-custom','LogsCustomController@all');
Route::get('/filter-logs','LogsCustomController@filterLogs');
Route::post('/add-logs','LogsCustomController@create');
Route::put('/update-logs/{id}','LogsCustomController@updateTransaction');
Route::delete('/delete-logs/{id}','LogsCustomController@deleteTransaction');

Route::get('/forex', 'ForexController@getUpdates');